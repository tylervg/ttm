<?php
ob_start();

session_start();

$site_path = realpath(dirname(__FILE__));
define ('SITE_PATH', $site_path);
define ('URLROOT', 'http://tmvc.local'); //no trailing slash
define ('SSLROOT', 'https://tmvc.local');
define ('SITE_TITLE', 'TMVC');
define( 'DB_HOST', 'localhost' );
define( 'DB_USER', 'root' );
define( 'DB_PASS', 'root' );
define( 'DB_NAME', 'project_manager' );
define( 'SEND_ERRORS_TO', 'tvaughn0@gmail.com' );
define( 'DISPLAY_DEBUG', true ); //display db errors?

require_once( SITE_PATH . '/tmvc-system/helper.php' );
require_once( SITE_PATH . '/tmvc-system/class.tmvc.php' );
require_once( SITE_PATH . '/tmvc-system/class.db.php' );
require_once( SITE_PATH . '/tmvc-system/class.auth.php' );
require_once( SITE_PATH . '/tmvc-model/class.model.php' );
require_once( SITE_PATH . '/tmvc-system/class.controller.php' );

include_once("tmvc-system/PFBC/Form.php");

$pathinfo = getPathinfo();

$configs 				= array();
$configs['ftpserver'] 	= '';
$configs['ftpusername'] = '';
$configs['ftppassword'] = '';

$Controller = new Controller($configs, $pathinfo);

// Route controllers manually
if ($pathinfo[1] == "" || $pathinfo[1]=='index.php')
	$Controller->home_controller();

else if ($pathinfo[1] == "phpinfo")
	phpinfo();

else $Controller->default_controller($pathinfo);
///////////


ob_end_flush();
