
var map, directionsDisplay, directionsService;

var latlng = new google.maps.LatLng(47.570525,-122.340188);

(function() {
	window.onload = function() {
		displayMap();
	}

})();


function displayMap(){

	var mapDiv = document.getElementById('map');
	var options = {
		center: latlng,
		zoom: 16,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};

	//var map = new google.maps.Map(mapDiv, options);
	map = new google.maps.Map(mapDiv, options);

	// Adding a marker to the map
	var marker = new google.maps.Marker({
		position: new google.maps.LatLng(47.570525,-122.340188),
		map: map,
		title: 'Relumin'
	});

	// Creating an InfoWindow with the content text: "Hello World"
	var infowindow = new google.maps.InfoWindow({
		content:	'<b>Relumin</b><br />' +
					'3623 East Marginal Way South<br />' +
					'Seattle, WA 98134'
	});

	// Adding a click event to the marker
	google.maps.event.addListener(marker, 'click', function() {
		// Calling the open method of the infoWindow
		infowindow.open(map, marker);
	});
}

