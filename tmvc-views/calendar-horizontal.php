<br/><br/><br/><br/>

 
  <?php

//$num = cal_days_in_month(CAL_GREGORIAN, 11, 2013); // 31
//echo "There $num days<br/>";
//echo date("h:i:s A");
//echo ' <br/>';

$calheight = 1020;

$numProjects = count($data['projects']);

$spacePerProj = $calheight / $numProjects;


//  $colors=rainbow('000000','FFFFFF',$numProjects);

$colors = array(
'8FB6D9',
'8AAED0',
'607990',
'354350',
'726541',
'B29E66',
'F2D68A',

'D8BF7B',
'222B33',
'3B3421',
'8AAED0',
'F2D68A',
'354350',
'B29E66',
'607990',
'726541',
'8FB6D9',
'D8BF7B',
'222B33',
'3B3421',
);
/*
'F2D68A',
'B7BFCC',
'6E9997',
'8C8A88',
'A6BFB7',
'5C5653',
'514E59',
'5C7274',
'8AAED0',
'F2D68A',
'B7BFCC',
'6E9997',
'8C8A88',
'A6BFB7',
'5C5653',
'514E59',
'5C7274'
*/


$currentMonth = date('n');
$currentYear  = date('Y');
$todayArr = explode('-',date("j-n-Y"));
$todayMysqlFormat = date("j-n-Y");


for($i=0; $i<count($data['projects']); $i++) {

	if($data['projects'][$i]['p_status']=='Paused') {
		//unset($data['projects'][$i]); //continue;
		//continue;
	}
	// for each project, set h position and color
	if($i==0) $nextPosition = 70;
	
	// h position is based on total cal height, divided by num of projects
	$data['projects'][$i]['top'] = $nextPosition;
	$data['projects'][$i]['height'] = $spacePerProj;
	$data['projects'][$i]['color'] = $colors[$i];
	
	$nextPosition = $spacePerProj + $nextPosition;
}


for($i=1; $i<=12; $i++) {
	if($i>=$currentMonth) {
		
		echo draw_calendar($i, $currentYear, $todayArr, $data);
	}
}

/* draws a calendar */
function draw_calendar($month,$year, $todayArr, $data){


	if(!isset($data)) exit('no data');

	/* draw month */
	$calendar = '<div class="month">';
	
	$monthname = date("M", mktime(0, 0, 0, $month));
	$calendar .= '<h2 class="calMonth">'.$monthname.' '.$year.'</h2>';

	/* headings */
	$headings = array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');

	/* days and weeks vars now ... */
	$running_day = date('w',mktime(0,0,0,$month,1,$year));
	$days_in_month = date('t',mktime(0,0,0,$month,1,$year));
	$days_in_this_week = 1;
	$day_counter = 0;
	$dates_array = array();
	
	/* row for week one */

	/* print "blank" days until the first of the current week */
	for($x = 0; $x < $running_day; $x++):
		//$calendar.= '<td class="calendar-day-np"></td>';
		$days_in_this_week++;
	endfor;

	/* keep going with days.... */
	for($list_day = 1; $list_day <= $days_in_month; $list_day++):
		//$calendar.= '<td class="calendar-day">';
			
			/* add in the day number */
			$dateInt = $list_day;
			
			//$dayOfWeek = echo jddayofweek ( cal_to_jd(CAL_GREGORIAN, date("m"),date("d"), date("Y")) , 1 ); 
			$weekDay = substr($headings[$running_day], 0, 2);
			$dayClass = ($weekDay=='Sa'|| $weekDay=='Su') ? 'weekend' : '';
			
			if ($todayArr[0]==$list_day && $todayArr[1]==$month && $todayArr[2]==$year){
				$todayClass = ' today';
			} else {
			
				$todayClass = '';
			}
			
			$calendar.= '<div class="calDay'.$todayClass.'">'."\n".'
					<span class="dayhead '.$dayClass.'">'.$weekDay.', '.$list_day.'</span>';

			if(!isset($data)) exit('no data');
			
			$tasks = '';
			for($p=0; $p<count($data['projects']); $p++) {
			
				$color = '#'.$data['projects'][$p]['color']; //getCSSColor($p); // 325345 //
			
				// if proj starts after today or ends before today, show info on first day or last day
				$showInfo = (($todayMysqlFormat<$data['projects'][$p]['p_start']) && ($data['projects'][$p]['p_start']== $year.'-'.$month.'-'.$list_day)) ? true: false;
				
				if($todayClass || $showInfo) $info = '<span class="moreInfo" rel="popover" data-trigger="hover" data-content="'.$data['projects'][$p]['p_summary'].'" data-original-title="'.$data['projects'][$p]['p_title'].'">
											<span class="taskTitle" style="background-color:'.$color.'">'.$data['projects'][$p]['p_title'].'</span>
										</span>';
				else $info = '';
				//$taskTitle = ($todayClass) ? '<div class="taskTitleVert">'.$data['projects'][$p]['p_title'].'</div>' : '';
			
				// today on or after start and on or before end
				$from = strtotime($data['projects'][$p]['p_start']);
				$to = strtotime($data['projects'][$p]['p_end']);
				$calDate = strtotime($year."-".$month."-".$list_day);
				if($from <= $calDate && $to >= $calDate) {
					
					
					
					//exit($color);
					
					$status = ($data['projects'][$p]['p_status']!='Current') ? 'border-top:2px dashed '.$color.';' : '';
					//$dim = ($data['projects'][$p]['p_status']!='Current') ? 'opacity:.4;' : '';
					
				    $tasks .= '<div class="projectCalDisplay" style="top:'.$data['projects'][$p]['top'].'px; height:'.$data['projects'][$p]['height'].'px;">
					    			<div style="position:relative;width:100%;height:100%;'.$dim.'">'.$taskTitle.'
						    			<div class="projectCalLine" data-tid="'.$data['projects'][$p]['p_id'].'" data-color="'.$color.'" style="border-top:10px solid pink;height:100%; border-color:'.$color.';'.$status.'">&nbsp;
											'.$info.' 
										</div>
									</div>
				    			</div>'."\n\t\t";
					
				} 
				
			
			}
			
			$calendar .= $tasks;
			
			
		
		if($running_day == 6){
			
			if(($day_counter+1) != $days_in_month){
				
			}
			$running_day = -1;
			$days_in_this_week = 0;
		}
		
			$calendar.= '</div><!-- end day-->';
		
		$days_in_this_week++; $running_day++; $day_counter++;
	endfor;

	/* finish the rest of the days in the week */
	/*
	if($days_in_this_week < 8){
		for($x = 1; $x <= (8 - $days_in_this_week); $x++){
		}
	}
	*/

	/* end the month */
	$calendar.= '</div><!-- end month -->';
	
	/* all done, return result */
	return $calendar;
}











