<br/><br/><br/><br/>

 
 <script>  
$(function ()  
{ $(".popover").popover({ trigger: "hover" });  
});  
</script>  

 
  <?php

//$num = cal_days_in_month(CAL_GREGORIAN, 11, 2013); // 31
//echo "There $num days<br/>";
//echo date("h:i:s A");
//echo ' <br/>';

$calWidth = 820;

$numProjects = count($data['projects']);

$spacePerProj = $calWidth / $numProjects;


//  $colors=rainbow('000000','FFFFFF',$numProjects);

$colors = array(
'8FB6D9',
'8AAED0',
'607990',
'354350',
'726541',
'B29E66',
'F2D68A',

'D8BF7B',
'222B33',
'3B3421',
'8AAED0',
'F2D68A',
'354350',
'B29E66',
'607990',
'726541',
'8FB6D9',
'D8BF7B',
'222B33',
'3B3421',
);
/*
'F2D68A',
'B7BFCC',
'6E9997',
'8C8A88',
'A6BFB7',
'5C5653',
'514E59',
'5C7274',
'8AAED0',
'F2D68A',
'B7BFCC',
'6E9997',
'8C8A88',
'A6BFB7',
'5C5653',
'514E59',
'5C7274'
*/


function getCSSColor($num) {
	
	/*
	$color = dechex($num) . dechex($num) . dechex($num+1) .dechex($num+1) . dechex($num+3) . dechex ($num+3);
	return '#'.$color;
	
	$num = $num*32452366;
	$hex = dechex ($num);
	return '#'.substr($hex,0,6);
	
	
    $hash = md5('5' . $num); // modify 'color' to get a different palette
    $cArr = array(
        hexdec(substr($hash, 0, 2)), // r
        hexdec(substr($hash, 2, 2)), // g
        hexdec(substr($hash, 4, 2))); //b
	return "rgb(".$cArr[0].",".$cArr[1].",".$cArr[2].")";
	*/
}

//exit(getCSSColor(23456789));


$currentMonth = date('n');
$currentYear  = date('Y');
$todayArr = explode('-',date("j-n-Y"));


for($i=0; $i<count($data['projects']); $i++) {

	if($data['projects'][$i]['p_status']=='Paused') {
		//unset($data['projects'][$i]); //continue;
		//continue;
	}
	// for each project, set h position and color
	if($i==0) $nextPosition = 70;
	
	// h position is based on total cal width, divided by num of projects
	$data['projects'][$i]['left'] = $nextPosition;
	$data['projects'][$i]['width'] = $spacePerProj;
	$data['projects'][$i]['color'] = $colors[$i];
	
	$nextPosition = $spacePerProj + $nextPosition;
	
}





for($i=1; $i<=12; $i++) {
	if($i>=$currentMonth) {
		
		display_month($i, $currentYear, $todayArr, $data);
	}
}



function display_month($month, $year, $todayArr, $data) {
	
	echo draw_calendar($month,$year,$todayArr, $data);
}

/* draws a calendar */
function draw_calendar($month,$year, $todayArr, $data){


	if(!isset($data)) exit('no data');

	/* draw table */
	//$calendar = '<table cellpadding="0" cellspacing="0" class="calendar">';
	$calendar = '<div class="month">';
	
	$monthname = date("M", mktime(0, 0, 0, $month));
	$calendar .= '<h2 class="calMonth">'.$monthname.' '.$year.'</h2>';

	/* table headings */
	$headings = array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');
	//$calendar.= '<tr class="calendar-row"><td class="calendar-day-head">'.implode('</td><td class="calendar-day-head">',$headings).'</td></tr>';

	/* days and weeks vars now ... */
	$running_day = date('w',mktime(0,0,0,$month,1,$year));
	$days_in_month = date('t',mktime(0,0,0,$month,1,$year));
	$days_in_this_week = 1;
	$day_counter = 0;
	$dates_array = array();
	
	

	/* row for week one */
	//$calendar.= '<tr class="calendar-row">';

	/* print "blank" days until the first of the current week */
	for($x = 0; $x < $running_day; $x++):
		//$calendar.= '<td class="calendar-day-np"></td>';
		$days_in_this_week++;
	endfor;

	/* keep going with days.... */
	for($list_day = 1; $list_day <= $days_in_month; $list_day++):
		//$calendar.= '<td class="calendar-day">';
			/* add in the day number */
			
			$dateInt = $list_day;
			
			//$dayOfWeek = echo jddayofweek ( cal_to_jd(CAL_GREGORIAN, date("m"),date("d"), date("Y")) , 1 ); 
			$weekDay = substr($headings[$running_day], 0, 2);
			$dayClass = ($weekDay=='Sa'|| $weekDay=='Su') ? 'weekend' : '';
			
			if ($todayArr[0]==$list_day && $todayArr[1]==$month && $todayArr[2]==$year){
				$todayId = 'id="today"';
			} else {
			
				$todayId = '';
			}
			
			$calendar.= "\n".'<div class="calDay" '.$todayId.'><span class="dayhead '.$dayClass.'">'.$weekDay.', '.$list_day.'</span>';
			
			if ($todayId) $calendar.=  '<div id="todayBG">TODAY</div>';

			/** QUERY THE DATABASE FOR AN ENTRY FOR THIS DAY !!  IF MATCHES FOUND, PRINT THEM !! **/
			//$calendar.= str_repeat('<p> </p>',2);
			
			
			// get project data for each day. 
			// for each project, define horizontal position and color before this point
			// add border-left if project happens on this day
			// add border-top if first day of project
			// add border0bottom is last day of project
			
			if(!isset($data)) exit('no data');
			//else print_r($data);
			
			$tasks = '';
			
			for($p=0; $p<count($data['projects']); $p++) {
			
				$color = '#'.$data['projects'][$p]['color']; //getCSSColor($p); // 325345 //
			
				//echo $data['projects'][$p]['p_start'].'<br/>';
				
				if($todayId) $info = '<span class="moreInfo" rel="popover" data-trigger="hover" data-content="'.$data['projects'][$p]['p_summary'].'" data-original-title="'.$data['projects'][$p]['p_title'].'">
										<!--<i class="icon-info-sign"></i>-->
										<div class="taskTitleVert" style="background-color:'.$color.'">'.$data['projects'][$p]['p_title'].'</div>
										</span>';
				else $info = '';
				//$taskTitle = ($todayId) ? '<div class="taskTitleVert">'.$data['projects'][$p]['p_title'].'</div>' : '';
			
				// today on or after start and on or before end
				$from = strtotime($data['projects'][$p]['p_start']);
				$to = strtotime($data['projects'][$p]['p_end']);
				$calDate = strtotime($year."-".$month."-".$list_day);
				if($from <= $calDate && $to >= $calDate) {
					
					
					
					//exit($color);
					
					$status = ($data['projects'][$p]['p_status']!='Current') ? 'border-left:2px dashed '.$color.';' : '';
					$dim = ($data['projects'][$p]['p_status']!='Current') ? 'opacity:.4;' : '';
					
				    $tasks .= '<div class="projectCalDisplay" style="left:'.$data['projects'][$p]['left'].'px; width:'.$data['projects'][$p]['width'].';">
					    			<div style="position:relative;width:100%;height:100%;'.$dim.'">'.$taskTitle.'
						    			<div class="projectCalLine" style="border-left:10px solid pink;height:100%; border-color:'.$color.';'.$status.'">&nbsp;
											'.$info.' 
										</div>
									</div>
				    			</div>'."\n\t\t";
					
				} 
				
			
			}
			
			$calendar .= $tasks;
			
			
		//$calendar.= '</td>';
		if($running_day == 6):
			//$calendar.= '</tr>';
			if(($day_counter+1) != $days_in_month):
				//$calendar.= '<tr class="calendar-row">';
			endif;
			$running_day = -1;
			$days_in_this_week = 0;
		endif;
		
			$calendar.= '</div>';
		
		$days_in_this_week++; $running_day++; $day_counter++;
	endfor;

	/* finish the rest of the days in the week */
	if($days_in_this_week < 8):
		for($x = 1; $x <= (8 - $days_in_this_week); $x++):
			//$calendar.= '<td class="calendar-day-np">&nbsp;</td>';
		endfor;
	endif;

	/* final row */
	//$calendar.= '</tr>';

	/* end the table */
	$calendar.= '</div>';
	
	/* all done, return result */
	return $calendar;
}










 function rainbow($start, $end, $steps)
 {
    $s=str_to_rgb($start);
    $e=str_to_rgb($end);
    $out=array();
    $r=(integer)($e['r'] - $s['r'])/$steps;
    $g=(integer)($e['g'] - $s['g'])/$steps;
    $b=(integer)($e['b'] - $s['b'])/$steps;
    for ($x=0; $x<$steps; $x++) {
       $out[]=rgb_to_str(
          $s['r']+(integer)($r * $x),
          $s['g']+(integer)($g * $x),
          $s['b']+(integer)($b * $x));
    }
    return $out;
 }
 function rgb_to_str($r, $g, $b)
 {
      return str_pad($r, 2, '0', STR_PAD_LEFT)
          .str_pad($g, 2, '0', STR_PAD_LEFT)
          .str_pad($b, 2, '0', STR_PAD_LEFT);
 }
 function str_to_rgb($str)
 {
    return array (
      'r'=>hexdec(substr($str, 0, 2)),
      'g'=>hexdec(substr($str, 3, 2)),
      'b'=>hexdec(substr($str, 5, 2))
    );
 }




