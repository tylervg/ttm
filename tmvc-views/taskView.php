

<?php

	echo '<h1>Task Notes</h1>';
	
	$statusArr = array('Current', 'Paused','Pending','Future');

?>
<form class="well" name="editTask" id="editTask">

	<div style="margin-bottom:10px;font-weight:bold"><?=$data['p_title']?></div>

	<div class="alert alert-success" id="savedAlert">Saved</div>
	
	<div>Status<br/>
		<select name="status" id="status">
		<?php
			foreach($statusArr as $status){
				$selected = ($status==$data['p_status']) ? ' selected="selected"' : '';
				echo '<option value="'.$status.'" '.$selected.'>'.$status.'</option>';
			}
		?>
		</select>
	</div>

	<div>
		Priority<br/>
		<input type="text" name="priority" id="priority" value="<?=$data['p_priority']?>">
	</div>

	<div>
		Timeline<br/>
		<input type="text" id="start" value="<?php echo ($data['p_start']!= '0000-00-00') ? convdate_short($data['p_start']) : '';?>">
		-
		<input type="text" id="end" value="<?php echo ($data['p_end']!= '0000-00-00') ? convdate_short($data['p_end']) : '';?>">
	</div>
	
	<div>
		Summary<br/>
		<textarea name="summary" id="summary" style="width:90%"><?=$data['p_summary']?></textarea>
	</div>
	
	<div>
		Notes<br/>
		<textarea name="notes" id="notes" style="width:90%; height:300px; background-color:#ffeeb4" autofocus="autofocus"><?=$data['p_notes']?></textarea>
	</div>
	
	<input type="hidden" name="tid" id="tid" value="<?=$data['p_id']?>">
	<input type="hidden" name="ajax" value="1">

</form>