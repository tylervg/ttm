<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>TTM Task MAnager</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">

    <link href="/assets/css/bootstrap.css" rel="stylesheet">
    <link href="/assets/css/bootstrap-responsive.css" rel="stylesheet">
    

	<link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Droid+Sans' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
	
	
	
	<link href="http://code.jquery.com/ui/1.10.3/themes/flick/jquery-ui.css" rel="stylesheet">
	<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
	<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="/assets/js/html5shiv.js"></script>
    <![endif]-->
    
    <link href="/assets/css/local.css" rel="stylesheet">
    
    <script type="text/javascript">
    	$(document).ready(function (){
    	
    		/*
    		 $(".today .moreInfo").click(function(){
			   alert('k');
			}); 
			*/
    	
    		//$('html, body').animate({ scrollLeft: $(".today").offset().top-255}, 300);
    		
    		//$('.calendar-container').animate({ scrollLeft: $(".month")}, 300);
    		$('.calendar-container').animate({ scrollLeft: $(".today").offset().left}, 300); // , {duration: 1000, easing: 'easeOutBounce'}
    		//$('.calendar-container').scrollLeft( 200 );
    		
    		
    		//$(".today").scrollLeft();
    		
    		//$("#today").scrollTop(200);
    		//$(".today .moreInfo").popover({placement : 'right'});
    		
    		
    		//projectCalLine
    		$( ".projectCalLine" ).hover(
				function() {
					var taskid = $( this ).attr('data-tid');
					var tcolor = $( this ).attr('data-color');
					var sidetask = $("#sidetask_"+taskid).css("font-weight","bold");
					var sidetask = $("#sidetask_"+taskid).css("border-left","4px solid "+tcolor);
					
					// sidetask_$id
					
				}, function() {
					//$( this ).removeClass( "hover" );
					var taskid = $( this ).attr('data-tid');
					var tcolor = $( this ).attr('data-color');
					var sidetask = $("#sidetask_"+taskid).css("font-weight","normal");
					var sidetask = $("#sidetask_"+taskid).css("border-left","4px solid #fff");
				}
			);
			
			$( ".projectCalLine" ).click(
				function() {
					var taskid = $( this ).attr('data-tid');
					//alert('task: '+taskid);
					//$( "#taskEdit" ).dialog( "open" );
					
					$("#taskModal").html("");
					$("#taskModal").dialog("option", "title", "Loading...").dialog("open");
					$("#taskModal").load("http://tasks.local/taskView?ajax=1&tid="+taskid, function() {
						$(this).dialog("option", "title", $(this).find("h1").text());
						$(this).find("h1").remove();
						$(this).find("#notes").focus();
					});
				}
			);
				
				
				
			$( "#taskModal" )
				.dialog({
					autoOpen: false,
					height: 810,
					width: 650,
					modal: true,
					buttons: [
						{
							text: "Save",
							"class": 'btn',
							click: function() {
						
								var tid 	= $('#tid').val();
								var start 	= $('#start').val();
								var end 	= $('#end').val();
								var summary = $('#summary').val();
								var notes 	= $('#notes').val();
								var status 	= $('#status').val();
								var priority= $('#priority').val();
								var taskData = { tid: tid, ajax: 1, start: start, end: end, summary: summary, notes: notes, status:status, priority:priority };
					            $.ajax({
					                type: "POST",
					                url: "/taskSave",
					                data: taskData,
					                datatype: "html",
					                success: function (data) {
					                    //$('#result').html(data);
					                    //alert('test '+data);
					                    $("#savedAlert").css('display','block');
					                    $("#savedAlert").fadeOut(2000, function() { $(this).css('display','none'); });
					                    location.reload();
					                }
					            });
					         }
						}
						
					],
					close: function() {
						//allFields.val( "" ).removeClass( "ui-state-error" );
					}
					
				});
			
			
			$('#datepicker').live('click', function() {
				//alert('test');
		        //$(this).datepicker({showOn:'focus'}).focus();
		    });
    		
        });

    </script>
    
    
  </head>

  <body>

    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>

		  <a class="brand" href="#">My Tasks</a>

          <div class="nav-collapse collapse">
            <ul class="nav mainnav">
            	<?php
            	if(is_array($navTree)) {
					foreach($navTree as $sectionLink => $sectionArray) {

						$active = ($pathinfo[1]==$sectionLink) ? 'active' : '';
						$sectionLink = ($pathinfo[1]=='') ? '' : $sectionLink;

						if($sectionArray['sectiontitle'])
							echo '<li class="'.$active.'"><a href="/dev/'.$sectionLink.'">'.$sectionArray['sectiontitle'].'</a></li>';
            		}
            	}
            	?>

            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>



		<div style="padding-left:30px;">
		

				<?=$content?>
				
	
		</div>


	<div style="clear:both"></div>
		<!-- Footer -->


    <script src="/assets/js/bootstrap.min.js"></script>

  </body>
</html>
