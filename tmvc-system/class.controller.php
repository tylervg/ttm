<?php

class Controller extends Tmvc{

	//
	// HOME PAGE
	function home_controller()
	{
		// get model data

		$data['projects'] = $this->Model->get_projects();
		//$contentData 	= $this->Model->get_home_content();
		//$navTree 		= $this->Model->getNavTree();

		//$pathinfo = $this->$pathinfo;


		$content = buffered_include('tmvc-views/home.php', $data);

		//echo SITE_PATH . '/tmvc-views/body.php';
		include_once( SITE_PATH . '/tmvc-views/body.php');
	}


	//
	// DEFAULT PAGES
	function default_controller()
	{
		$pathinfo = $this->pathinfo;
		
		if($pathinfo[1]=='taskSave') {
			
			$update = $this->Model->update_project();
			echo $update;
		}
		
		if($_REQUEST['tid']) $data = $this->Model->get_project($_REQUEST['tid']);
		
		//$data = array();
		//$navTree 	= $this->Model->getNavTree();
		//$data 		= $this->Model->get_content();
		//$data['navTree'] = $navTree;
		//$data['pathinfo'] = $pathinfo;

		//print_r($pathinfo);
		//exit(SITE_PATH);


		if(!$filepath){
			$pathstr = 'tmvc-views';
			if($pathinfo[2]) 	$filepath = $pathstr.'/'.$pathinfo[1].'/'.$pathinfo[2].'.php';
			else 				$filepath = $pathstr.'/'.$pathinfo[1].'.php';
		}
		
		//exit($filepath);
		

		$filepath = ( file_exists($filepath) ) ? $filepath : '/tmvc-views/default.php';

		$content = buffered_include($filepath, $data);
		
		if($_REQUEST['ajax']) echo $content;

		else include_once( SITE_PATH . '/tmvc-views/body.php');
	}



} //end class
?>