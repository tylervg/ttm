<?php

	// Helper functions


	//QUOTE_SMART
	// set $noquotes to true if you want to force not quoting
	function quote_smart($value, $noquotes=false)
	{
	  // Stripslashes
	  if (get_magic_quotes_gpc()) { $value = stripslashes($value); }

	  // Quote if not integer
	  if (!is_numeric($value)){
		$value = ($noquotes) ? mysql_escape_string($value) : "'" . mysql_escape_string($value) . "'";
	  }

	  // prevent XSS
	  $value = htmlentities($value);
	  return $value;
	}

	function session($name)
	{
		return $_SESSION[$name];
	}

	//EMAIL CHECKER. THERE ARE BETER ONES.
	function check_email_address($email) {
		// First, we check that there's one @ symbol, and that the lengths are right
		if (!ereg("^[^@]{1,64}@[^@]{1,255}$", $email))
		{
				// Email invalid because wrong number of characters in one section, or wrong number of @ symbols.
				return false;
			}
			// Split it into sections to make life easier
			$email_array = explode("@", $email);
			$local_array = explode(".", $email_array[0]);
			for ($i = 0; $i < sizeof($local_array); $i++) {
				if (!ereg("^(([A-Za-z0-9!#$%&'*+/=?^_`{|}~-][A-Za-z0-9!#$%&'*+/=?^_`{|}~\.-]{0,63})|(\"[^(\\|\")]{0,62}\"))$", $local_array[$i])) {
					return false;
				}
			}
			if (!ereg("^\[?[0-9\.]+\]?$", $email_array[1])) { // Check if domain is IP. If not, it should be valid domain name
				$domain_array = explode(".", $email_array[1]);
				if (sizeof($domain_array) < 2) {
					return false; // Not enough parts to domain
				}
				for ($i = 0; $i < sizeof($domain_array); $i++) {
					if (!ereg("^(([A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9])|([A-Za-z0-9]+))$", $domain_array[$i])) {
					return false;
				}
			}
		}
		return true;
	}


	//PASSWORD ENCRYPTION
	function encrypt_pass($toencode, $times=1000)
	{
		$salt = 't+}{v$';
		for( $i=0; $i<$times; $i++ )
		{
			$toencode = hash('sha512', $salt.$toencode);
			$toencode = md5($toencode.$salt);
		}
		return $toencode;
	}


	function prep_url($str = '')
	{
		if ($str == 'http://' OR $str == '')
		{
			return '';
		}

		if (substr($str, 0, 7) != 'http://' && substr($str, 0, 8) != 'https://')
		{
			$str = 'http://'.$str;
		}

		return $str;
	}


	function safe_mailto($email, $title = '', $attributes = '')
	{
		$title = (string) $title;

		if ($title == ""){
			$title = $email;
		}

		for ($i = 0; $i < 16; $i++){
			$x[] = substr('<a href="mailto:', $i, 1);
		}

		for ($i = 0; $i < strlen($email); $i++){
			$x[] = "|".ord(substr($email, $i, 1));
		}

		$x[] = '"';

		if ($attributes != '')
		{
			if (is_array($attributes))
			{
				foreach ($attributes as $key => $val)
				{
					$x[] =  ' '.$key.'="';
					for ($i = 0; $i < strlen($val); $i++)
					{
						$x[] = "|".ord(substr($val, $i, 1));
					}
					$x[] = '"';
				}
			}
			else
			{
				for ($i = 0; $i < strlen($attributes); $i++)
				{
					$x[] = substr($attributes, $i, 1);
				}
			}
		}

		$x[] = '>';

		$temp = array();
		for ($i = 0; $i < strlen($title); $i++)
		{
			$ordinal = ord($title[$i]);

			if ($ordinal < 128)
			{
				$x[] = "|".$ordinal;
			}
			else
			{
				if (count($temp) == 0)
				{
					$count = ($ordinal < 224) ? 2 : 3;
				}

				$temp[] = $ordinal;
				if (count($temp) == $count)
				{
					$number = ($count == 3) ? (($temp['0'] % 16) * 4096) + (($temp['1'] % 64) * 64) + ($temp['2'] % 64) : (($temp['0'] % 32) * 64) + ($temp['1'] % 64);
					$x[] = "|".$number;
					$count = 1;
					$temp = array();
				}
			}
		}

		$x[] = '<'; $x[] = '/'; $x[] = 'a'; $x[] = '>';

		$x = array_reverse($x);
		ob_start();

	?><script type="text/javascript">
	//<![CDATA[
	var l=new Array();
	<?php
	$i = 0;
	foreach ($x as $val){ ?>l[<?php echo $i++; ?>]='<?php echo $val; ?>';<?php } ?>

	for (var i = l.length-1; i >= 0; i=i-1){
	if (l[i].substring(0, 1) == '|') document.write("&#"+unescape(l[i].substring(1))+";");
	else document.write(unescape(l[i]));}
	//]]>
	</script><?php

		$buffer = ob_get_contents();
		ob_end_clean();
		return $buffer;
	}



	//GET FILE ICON
	function get_file_icon($file=false)
	{
		$filetype = $this->getExtension($file);

		switch($filetype) {
			  case 'PDF' :
				$icon = array('title'=>'PDF Document', 'icon'=>'icon-pdf.gif');
				break;
			  case 'DOC' :
				$icon = array('title'=>'Word Document', 'icon'=>'icon-doc.gif');
				break;
			  case 'XLS' :
				$icon = array('title'=>'Excel Document', 'icon'=>'icon-xls.gif');
				break;
			  case 'PPT' :
				$icon = array('title'=>'PowerPoint Document', 'icon'=>'icon-ppt.gif');
				break;
			  default:
			  	$icon=false;
		}

		return $icon;
	}


	function getExtension($file=false)
	{
		$pos = strrpos($file, ".");
		if ($pos) {
			$ext = strtolower(substr($file, $pos, strlen($file)));
			switch($ext) {
			  case '.jpg' :
				$ext='.jpg';
				break;
			  case '.pdf' :
				$ext="PDF";
				break;
			  case '.doc' :
				$ext="DOC";
				break;
			  case '.xls' :
				$ext="XLS";
				break;
			  case '.ppt' :
				$ext="PPT";
				break;
			  default:
			  	$ext=false;
			}
		}
		$ext = strtoupper($ext);

		return $ext;
	}


	// DATE HANDLING
	function convdate($mysqldate)
	{
		list($year,$month,$day)=explode("-",$mysqldate);
		$display_date=date("F d, Y",mktime(0,0,0,$month,$day,$year));
		return $display_date;
	}

	function getmonth($mysqldate)
	{
		list($year,$month,$day)=explode("-",$mysqldate);
		$display_date=date("F",mktime(0,0,0,$month,$day,$year));
		return $display_date;
	}

	function convdate_short($mysqldate)
	{
		list($year,$month,$day)=explode("-",$mysqldate);
		$display_date=date("m/d/Y",mktime(0,0,0,$month,$day,$year));
		return $display_date;
	}

	function dayOfWeek($mysqldate)
	{
		list($year,$month,$day)=explode("-",$mysqldate);
		$display_date=date("D",mktime(0,0,0,$month,$day,$year));
		return $display_date;
	}


	//CONVERT SMART QUOTES
	function convert_smart_quotes($string)
	{
		$search = array(chr(145),
						chr(146),
						chr(147),
						chr(148),
						chr(151));

		$replace = array("'",
						 "'",
						 '"',
						 '"',
						 '-');

		return str_replace($search, $replace, $string);
	}


	//CONVERTS SPECIAL CHARACTERS TO HTML ENTITIES.
	//DIFF THAN PHP htmlentities, WONT STRIP OUT HTML TAGS
	function encodeEntities ( $string )
	{
		$trans_tbl = array() ;

		$trans_tbl[chr(34)] = '&#34;' ;		// 	quote
		//$trans_tbl[chr(38)] = '&#38;' ;		// 	ampersand
		//$trans_tbl[chr(60)] = '&#60;' ;		// 	less-than
		//$trans_tbl[chr(62)] = '&#62;' ;		// 	more-than
		$trans_tbl[chr(128)] = '&#8364;' ;	// 	euro
		$trans_tbl[chr(129)] = '&#8364;' ; 	//	euro
		$trans_tbl[chr(130)] = '&#8218;' ; 	//	low quote
		$trans_tbl[chr(131)] = '&#402;' ; 	//	florin
		$trans_tbl[chr(132)] = '&#8222;' ; 	// 	double low quote
		$trans_tbl[chr(133)] = '&#8230;' ; 	//	ellipsis
		$trans_tbl[chr(134)] = '&#8224;' ;	//	dagger
		$trans_tbl[chr(135)] = '&#8225;' ; 	//	double dagger
		$trans_tbl[chr(136)] = '&#710;' ; 	//	circumflex
		$trans_tbl[chr(137)] = '&#8240;' ; 	//	per thousand
		$trans_tbl[chr(138)] = '&#352;' ; 	//	S caron
		$trans_tbl[chr(139)] = '&#8249;' ; 	//	left angle quote
		$trans_tbl[chr(140)] = '&#338;' ; 	//	OE ligature
		$trans_tbl[chr(142)] = '&#381;' ; 	//	Z caron
		$trans_tbl[chr(145)] = '&#8216;' ; 	//	left single quote
		$trans_tbl[chr(146)] = '&#8217;' ; 	//	right single quote
		$trans_tbl[chr(147)] = '&#8220;' ; 	//	left double quote
		$trans_tbl[chr(148)] = '&#8221;' ; 	//	right double quote
		$trans_tbl[chr(149)] = '&#8226;' ; 	//	bullet
		$trans_tbl[chr(150)] = '&#8211;' ; 	//	en dash
		$trans_tbl[chr(151)] = '&#8212;' ; 	//	em dash
		$trans_tbl[chr(152)] = '&#732;' ; 	//	small tilde
		$trans_tbl[chr(153)] = '&#8482;' ; 	//	trademark
		$trans_tbl[chr(154)] = '&#353;' ; 	//	small s caron
		$trans_tbl[chr(155)] = '&#8250;' ; 	//	right angle quote
		$trans_tbl[chr(156)] = '&#339;' ; 	//	oe ligature
		$trans_tbl[chr(158)] = '&#382;' ; 	// 	small z caron
		$trans_tbl[chr(159)] = '&#376;' ; 	//	Y with diaeresis

		for ( $i=160; $i<=255; $i++ ) {
			$trans_tbl[chr($i)] = '&#' . $i . ';' ;
		}

		return strtr ( $string , $trans_tbl ) ;
	}


	function preprint($arr)
	{
		echo '<pre>';
		if(is_array($arr)) {
			print_r($arr);
		} else {
			echo '<br/>'.$arr.' NOT ARRAY<br/>';
		}
		echo '</pre>';
	}


	function statesArray ()
	{

			$states = array (
			'0' => ' - - - ',
			'AL' => 'Alabama',
			'AK' => 'Alaska',
			'AZ' => 'Arizona',
			'AR' => 'Arkansas',
			'CA' => 'California',
			'CO' => 'Colorado',
			'CT' => 'Connecticut',
			'DE' => 'Delaware',
			'DC' => 'District of Columbia',
			'FL' => 'Florida',
			'GA' => 'Georgia',
			'GU' => 'Guam',
			'HI' => 'Hawaii',
			'ID' => 'Idaho',
			'IL' => 'Illinois',
			'IN' => 'Indiana',
			'IA' => 'Iowa',
			'KS' => 'Kansas',
			'KY' => 'Kentucky',
			'LA' => 'Louisiana',
			'ME' => 'Maine',
			'MD' => 'Maryland',
			'MA' => 'Massachusetts',
			'MI' => 'Michigan',
			'MN' => 'Minnesota',
			'MS' => 'Mississippi',
			'MO' => 'Missouri',
			'MT' => 'Montana',
			'NE' => 'Nebraska',
			'NV' => 'Nevada',
			'NH' => 'New Hampshire',
			'NJ' => 'New Jersey',
			'NM' => 'New Mexico',
			'NY' => 'New York',
			'NC' => 'North Carolina',
			'ND' => 'North Dakota',
			'OH' => 'Ohio',
			'OK' => 'Oklahoma',
			'OR' => 'Oregon',
			'PA' => 'Pennsylvania',
			'RI' => 'Rhode Island',
			'SC' => 'South Carolina',
			'SD' => 'South Dakota',
			'TN' => 'Tennessee',
			'TX' => 'Texas',
			'UT' => 'Utah',
			'VT' => 'Vermont',
			'VA' => 'Virginia',
			'WA' => 'Washington',
			'WV' => 'West Virginia',
			'WI' => 'Wisconsin',
			'WY' => 'Wyoming');

			return $states;
	}


	//GET REAL IP
	//FROM: http://roshanbh.com.np/2007/12/getting-real-ip-address-in-php.html
	function getRealIpAddr()
	{
		if (!empty($_SERVER['HTTP_CLIENT_IP'])){   //check ip from share internet

		  $ip=$_SERVER['HTTP_CLIENT_IP'];

		} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){   //to check ip is pass from proxy

		  $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];

		} else {

		  $ip=$_SERVER['REMOTE_ADDR'];

		}
		return $ip;
	}


	function isAbsolutePath ($url)
	{
		if($url == NULL) return false;
		else {
			$pos = strpos($url, "http://");
			if ($pos === false)
				$pos = strpos($url, "https://");

			if ($pos === false || $pos > 0) return false;
			else return true;
		}
	}


	// PURPOSE:	compare a serialized value against an array and return array based on overlapping ids
	function unserialize_finder($serializedValue, $dataArray, $key)
	{

		$found = false;

		for($i=0; $i<count($dataArray); $i++){

			$unser = unserialize($serializedValue);
			if(!is_array($unser)) continue;

			if ( array_search( $dataArray[$i][$key], $unser ) === false) {

			} else {

				$found[] = $dataArray[$i];

			}

		}

		return $found;
	}


	// url maker function, remove duplicated vars
	// makeUrl('index.php', $_SERVER['QUERY_STRING'], 'name=value&name2=value2');
	function makeUrl($path, $qs = false, $qsAdd = false)
	{
		$var_array = array();
		$varAdd_array = array();
		$url = $path;

		if($qsAdd)
		{
			$varAdd = explode('&', $qsAdd);
			foreach($varAdd as $varOne)
			{
				$name_value = explode('=', $varOne);

				$varAdd_array[$name_value[0]] = $name_value[1];
			}
		}

		if($qs)
		{
			$var = explode('&', $qs);
			foreach($var as $varOne)
			{
				$name_value = explode('=', $varOne);

				//remove duplicated vars
				if($qsAdd)
				{
					if(!array_key_exists($name_value[0], $varAdd_array))
					{
						$var_array[$name_value[0]] = $name_value[1];
					}
				}
				else
				{
					$var_array[$name_value[0]] = $name_value[1];
				}
			}
		}

		//make url with querystring
		$delimiter = "?";

		foreach($var_array as $key => $value)
		{
			$url .= $delimiter.$key."=".$value;
			$delimiter = "&";
		}

		foreach($varAdd_array as $key => $value)
		{
			$url .= $delimiter.$key."=".$value;
			$delimiter = "&";
		}

		return $url;
	}


	// ------------------------------------------------------------------------
	function getPathinfo()
	{
		return explode('/',strtok($_SERVER['REQUEST_URI'], '?'));
	}


	// ------------------------------------------------------------------------
	function buffered_include($filepath, $data)
	{
		$file = SITE_PATH . '/'.$filepath;

		if(file_exists($file)) {

			ob_start();
			include($file);
			$buffer = ob_get_contents();
			ob_end_clean();
			return $buffer;

		} else {
			return false;
		}
	}


	// ------------------------------------------------------------------------
	// include view file into buffer based on pathinfo
	// if no view exists, returns false
	function auto_view_routing( $pathinfo )
	{
		$pathstr = 'tmvc-views';

		for($i=1; $i<count($pathinfo); $i++){

			$pathstr .= '/' . $pathinfo[$i];
		}

		if(file_exists($pathstr . '/index.php')) {

			$file = $pathstr . '/index.php';

		} else if(file_exists($pathstr . '.php')) {

			$file = $pathstr . '.php';

		} else {

			return false;
		}

		return buffered_include($file);
	}

?>