<?php

class Auth
{
	public static $uniqueID = 'rgrGRp~45GSdg74t857wq5@#$637R.TJDSGETw4etrw'; // used for remember-me cookie
	public static $DB;

	public function __construct( $DB )
	{
		$this->DB = $DB;
	}

	public function authRequired($level=1)
	{
		if (!session('uid')) {
			$this->logout();
			$this->redirectToLogin();
		}
		elseif (session('role')<$level){
			exit('Sorry, you do not have sufficient permissions to view this page.');
		}
		elseif (session('role')>=$level){
			//print_r($_SESSION);
			//exit();
			return true;
		}
		else {
			exit('Authentication error');
		}
	}

	public function redirectToLogin()
	{
		header('Location: /');
		exit('Please log in');
	}

	public function login($username, $password)
	{
		$sql = 'SELECT tdcms_uid, tdcms_role FROM tdcms_users WHERE tdcms_username = "'.$username.'" AND tdcms_password = "'.$password.'" LIMIT 1;';
		//echo $sql;

		if( $this->DB->num_rows( $sql ) > 0 )
		{
		    list( $uid, $role ) = $this->DB->get_row( $sql );

			unset($_SESSION['pfbc']); //clears pfbc data. might cause problems later. ?

			$_SESSION['uid'] = $uid;
			$_SESSION['role'] = $role;

			header('Location: /admin');
		}
		else
		{
		    exit('not found');
		}



		/*
		// check user and pass
		//exit("SELECT * FROM userlogin WHERE u_login = ".$Helper->quote_smart($user)." AND u_password = ".$Helper->quote_smart($pass));

		if ((isset($userRecord))&&(isset($userRecord['u_id']))&&($userRecord['u_login'] == $user)&&($userRecord['u_password'] == $pass)) {
			$this->isLoggedIn = 1;
			$_SESSION['userId'] = $userRecord['u_id'];
			$_SESSION['userLogin'] = $userRecord['u_login'];
			$_SESSION['userName'] = $userRecord['u_login'];

			// persistent 'remember me' cookie
			// this cookie is the user's id number, and the user's id number hashed with a secret key ($uniqueID)
			// this works because neither the user nor potential hackers know $uniqueID so they could not create
			// a matching id and hash pair.
			setcookie('VerifyUser', $userRecord['id']."--".md5($userRecord['id'].$uniqueID), time()+60*60*24*365);

			return 1;
		} else {
			return 0;
		}
		*/

	}

	function logout()
	{
		// end session
		$this->isLoggedIn = 0;
		$this->userId = "";
		$this->userLogin = "";
		$this->userName = "";
		session_unset();
		session_destroy();
		//setcookie('VerifyUser', "", time()-60*60*24*365);
		return 1;
	}






	public function checkLogin()
	{
		if (!isset($_SESSION['uid'])) {

			if ($_COOKIE['VerifyUser']) {
			/*
				// the cookie is split into the user id and the hashed id+uniqueID
				$Cut = explode("--", $_COOKIE['VerifyUser']);

				// if the hash in the cookie matches the id hashed with our secret key, it must be valid
				if (md5($Cut[0].$uniqueID) === $Cut[1]) {
					// user had valid remember-me cookie

					//exit("SELECT * FROM userlogin WHERE id = ".$Helper->quote_smart($Cut[0]));

					$userRec = @mysql_fetch_assoc(mysql_query("SELECT * FROM userlogin WHERE id = ".quote_smart($Cut[0])));
					if (isset($userRec['id'])&&($userRec['id'] != "")) {
						// valid user found in db
						$this->isLoggedIn = 1;
						$_SESSION['userId'] = $userRec['id'];
						$_SESSION['userLogin'] = $userRec['login'];
						$_SESSION['userName'] = $userRec['name'];
					} else {
						// user had valid cookie but was not found in db
						$this->isLoggedIn = 0;
					}
				} else {
					// user had invalid remember-me cookie
					$this->isLoggedIn = 0;
				}
				*/
			} else {
				// user didn't have a remember-me cookie
				return false;
			}
		} else {
			// user had existing session
			return true;
		}
	}

}

?>