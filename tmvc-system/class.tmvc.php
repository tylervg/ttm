<?php

class Tmvc
{
	public static $configs;
	public static $pathinfo;
	public $DB;
	public $Auth;
	public $Model;

	public function __construct( $configs, $pathinfo )
	{
       $this->configs = $configs;
       $this->pathinfo = $pathinfo;

       // $this->Queries = new Queries($configs, $pathinfo);
	   $this->DB = new DB();
	   //$this->Auth = new Auth($this->DB);
	   $this->Model = new Model($this->DB, $configs, $pathinfo);


	   if( isset( $_POST ) )
		{
		    foreach( $_POST as $key => $value )
		    {
		        $_POST[$key] = $this->DB->filter( $value );
		    }
		}

		if( isset( $_GET ) )
		{
		    foreach( $_GET as $key => $value )
		    {
		        $_GET[$key] = $this->DB->filter( $value );
		    }
		}
	}

    public function __get($property)
    {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value)
    {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }
	/*
    public function getNavTree()
    {
		//$nav = $this->Queries->getMainNav();
		//$nav = $this->Queries->getNav($type);

		//	$menutree[ $r[$i]['c_section'] ][ 'sectiontitle' ] = $r[$i]['c_title'];
		//	$menutree[ $r[$i]['c_section'] ][ $r[$i]['c_subsection'] ] = $r[$i]['c_title'];
		//	$menutree[ 'about' ][ 'sectiontitle' ] = 'About Us';
		//	$menutree[ 'about' ][ 'staff' ] = 'Staff';


		$menuTree = array();
		$prevMainCat = "";

		for($x = 0; $x < count($nav); $x++) {
			if($nav[$x]['url3'] != ""){	// 3rd level nav
				//echo("<br />3rd level");
				if(!isset($menuTree[$nav[$x]['url1']]['subSection'][$nav[$x]['url2']]['subSection'])){
					$menuTree[$nav[$x]['url1']]['subSection'][$nav[$x]['url2']]['subSection'] = array();
					$menuTree[$nav[$x]['url1']]['subSection'][$nav[$x]['url2']]['subSection'][$nav[$x]['url3']] = array();
				}

				$menuTree[$nav[$x]['url1']]['subSection'][$nav[$x]['url2']]['subSection'][$nav[$x]['url3']]['sectionTitle'] = $nav[$x]['navTitle'];

			} else if($nav[$x]['url2'] != ""){	// 2nd level nav
				//echo("<br />2nd level");
				if(!isset($menuTree[$nav[$x]['url1']]['subSection'])){
					$menuTree[$nav[$x]['url1']]['subSection'] = array();
					$menuTree[$nav[$x]['url1']]['subSection'][$nav[$x]['url2']] = array();
				}

				$menuTree[$nav[$x]['url1']]['subSection'][$nav[$x]['url2']]['sectionTitle'] = $nav[$x]['navTitle'];

			}else if($nav[$x]['url1'] != ""){	// 1st level nav
				//echo("<br />1st level");
				$menuTree[$nav[$x]['url1']] = array();
				$menuTree[$nav[$x]['url1']]['sectionTitle'] = $nav[$x]['mcName'];
			}


		}
		//Main::prePrint($menuTree, "menuTree [getMainNav]");
		return $menuTree;
	}
		*/


	// menutree from micronics::

	/*


			$sql = "
					SELECT c_id, c_title, c_section, c_level2, c_level3
					FROM content
					".$sqlWhere."
					ORDER BY FIELD(c_section,
					".$sqlFieldOrder."
					), c_sequence
		";



			$re = mysql_query($sql);
			while($row=mysql_fetch_assoc($re)){
				if($row['c_section']=='investor-relations'){

				}
				$r[] = $row;
			}


			//get PRODUCTS
			$prods = get_products_and_types();


			for($i=0; $i<count($r); $i++){

				//IF SECTION main && NOT PREVIOUS
				if ( ($r[$i]['c_level2']=='') && ($prev_section != $r[$i]['c_section']) ) {

					$menutree[ $r[$i]['c_section'] ][ 'sectiontitle' ] = $r[$i]['c_title'];

					//PRODUCTS
					if($r[$i]['c_section']=='products') {

						for($k=0; $k<count($prods); $k++){

							if($prods[$k]['pt_type'] != $prev_type) {

								$menutree[ $r[$i]['c_section'] ][ $prods[$k]['pt_nav_slug'] ]['level2Title'] = $prods[$k]['pt_nav_text'];

							}

							$menutree[ $r[$i]['c_section'] ][ $prods[$k]['pt_nav_slug'] ][ $prods[$k]['p_nav_slug'] ] = $prods[$k]['p_nav_text'];

							$prev_type = $prods[$k]['pt_type'];

						}

					}


				} else {

					//if level2 nd no level3
					if($r[$i]['c_level3']=='') $menutree[ $r[$i]['c_section'] ][ $r[$i]['c_level2'] ]['level2Title'] = $r[$i]['c_title'];


					elseif($r[$i]['c_level3']) {

						if($r[$i]['c_level3']) $menutree[ $r[$i]['c_section'] ][ $r[$i]['c_level2'] ][$r[$i]['c_level3']] = $r[$i]['c_title'];

					}

				}

				$prev_section = $r[$i]['c_section'];

			}



		return $menutree;

		*/





}