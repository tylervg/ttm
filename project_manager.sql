SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;


CREATE TABLE files (
  f_id int(11) NOT NULL AUTO_INCREMENT,
  f_file int(11) NOT NULL,
  f_pid int(11) NOT NULL COMMENT 'fk project id',
  PRIMARY KEY (f_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE projects (
  p_id int(11) NOT NULL AUTO_INCREMENT,
  p_title varchar(255) NOT NULL,
  p_start date NOT NULL,
  p_end date NOT NULL,
  p_summary text NOT NULL,
  p_notes text NOT NULL,
  p_status varchar(64) NOT NULL,
  p_priority int(11) NOT NULL DEFAULT '7',
  PRIMARY KEY (p_id),
  KEY p_start (p_start,p_end)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

INSERT INTO projects (p_id, p_title, p_start, p_end, p_summary, p_notes, p_status, p_priority) VALUES
(1, 'Web Dev Workflow', '2013-11-14', '2014-01-31', '-setting up dev servers\n-initializing git repos\n-git learning (workflow and merges)\n-drupal learning', '-set up iris dev\n-', 'Current', 4),
(2, 'Drupal Updates', '2013-12-09', '2013-12-20', '-libweb security patches\n-iris', '-try libweb updates on dev first\n-once iris has staging, try to upgrade iris to 7\n-think about retheming iris into bootstrap', 'Pending', 7),
(3, 'HTML GIS Maps', '2013-11-25', '2013-12-07', '-Contact Kathy about possibility of implementing GIS maps as html rather than flash. (easier to maintain and xplatform supported)', '-contacted Kathy on 12/5/13', 'Paused', 7),
(4, 'Features Design Upgrade', '2013-12-16', '2013-12-31', 'Barbara mentioned possibility of improving feature layout', '', 'Pending', 7),
(5, 'Booking Modeule', '2013-12-24', '2013-12-31', 'Sara mentioned media lending needs reporting improvements', '', 'Pending', 7),
(6, 'Templating', '2013-11-19', '2013-12-31', 'various template updates', 'TO DO\n-Janus sub pages (try on staging)\n-Worldcat (Sara)\n//-Illiad (remote ms) finished 12/17\n-FindText (reset uo password, log in to sfx)\n//FINISHED 12/16 -ScholarsBank (Java app)\n-Wordpress stuff (???)', 'Current', 1),
(7, 'Research Guides System', '2014-01-30', '2014-06-30', '(Sara and Duncan)\r\nImplementing guide management system such as lib-guides', '', 'Pending', 7),
(8, 'Display Page Ownership', '2013-12-02', '2014-01-10', 'Add dropdown field to Basic Page for assigning ownership to a page. This will be displayed lightly at the bottom of a content page. Assigning pages for past pages where appropriate, but normative for new pages.', 'Possibly implement this along with new content type', 'Current', 4),
(9, 'Accessibility Improvements', '2013-12-02', '2014-01-31', 'Contact James Bailey @ Disability Services to see what they actually need. ', '', 'Pending', 7),
(10, 'Content Writing Guidelines', '2013-12-02', '2014-01-12', '-URL guidelines\n-New Content Type link list, 255 char page summary, \n-3 col layout\n-pics, side nav', '-start with new content type on dev', 'Current', 5),
(11, 'Iris Internal Survey', '2013-11-18', '2013-12-11', 'Create lists of questions to see what people use on iris, and features they want to see. \n-Send Staff Announcement\n-Post on iris blog', '-Sent out!\n-On Dec 11, need to blog post and send email to staff lists\n-Dec 10, emailed info to Jan for Bulletin\n-Sara approved form monday Dec 9', 'Paused', 2),
(12, 'Libweb Photography Resource', '2013-12-16', '2014-01-31', '-New Image Field\r\n-visually browse and select from avail photos organized by category\r\n-get current avail photos from lesley\r\n-work w barb to categorize\r\n-add drupal interface\r\n-look into allowing social photo db\r\n-licensing issues, etc\r\n-2x images avail for retina displays \r\n-create content layouts', '', 'Pending', 7),
(13, 'Google search guides', '2014-01-02', '2014-01-03', 'integrate google guide search into page better.\nlook into possible licensing reqs', '', 'Current', 7),
(14, 'ILS tasks', '2014-01-13', '2014-06-30', 'Various ILS related work including homepage design, integrating with shibolith', '', 'Future', 7),
(15, 'ILS Search Wireframes', '2013-12-03', '2013-12-12', '-Create comps of simple ils search with multiple dropdown option formats\n-create search area in 3 different layouts (accordion, tabbed, text)', '-created pdf and attached to basecamp dec12\n\n-get examples of other primo websites (basecamp?)\n\nto do before next webdev meeting or by this friday 12/13', 'Paused', 1),
(16, 'PRIMO API documentation', '2013-12-03', '2014-01-20', 'Look at PRIMO api documentation. See Sara', '-emailed Sara 12/5', 'Paused', 7),
(17, 'StudyScape', '2013-12-12', '2014-02-14', 'Takeover of CMET study space tracking app', '-contacted will for sandbox 12.12\n-Get sandbox version from Will\n-Set up technical meeting with Will (and J and D?) to go over specs\n-Linda (dsc backend) and Dave (frontend) are current developers\n-Kirstin has ownership', 'Current', 3),
(18, 'Decode Iris search index logic', '2013-12-12', '2014-01-31', 'find out how iris search index currently works. notify content creators', '', 'Current', 3),
(19, 'libproxy.uoregon.edu', '2013-12-13', '2014-01-04', '-make this page more usable\n-dont display vpn info to people on campus', '', 'Current', 2);

CREATE TABLE stickies (
  s_id int(11) NOT NULL AUTO_INCREMENT,
  s_project int(11) NOT NULL COMMENT 'fk project id',
  s_text text NOT NULL,
  s_weight int(11) NOT NULL,
  s_date date NOT NULL,
  PRIMARY KEY (s_id),
  KEY s_project (s_project)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
         