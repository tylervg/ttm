<?php

class Model {

	public $DB;
	public $configs;
	public static $pathinfo;

	public function __construct( $DB, $configs, $pathinfo ) {

		$this->DB = $DB;
        $this->configs = $configs;
        $this->pathinfo = $pathinfo;
    }




	public function get_projects()
	{
		$sql = 'SELECT * FROM projects 
				ORDER BY FIELD(p_status, "Current", "Paused", "Pending", "Future"), p_priority, p_end';
		if( $this->DB->num_rows( $sql ) > 0 ) {
			return $this->DB->get_results( $sql );
		} else return false;
	}
	
	
	public function get_project($tid)
	{
		$sql = 'SELECT * FROM projects 
				WHERE p_id = '.$this->DB->filter($tid).' LIMIT 1';
		if( $this->DB->num_rows( $sql ) > 0 ) {
			$data = $this->DB->get_results( $sql );
			return $data[0];
		} else return false;
	}
	
	
	public function update_project()
	{
		/*
		$sql = 'UPDATE projects 
				SET p_start = "'.$this->DB->filter($_REQUEST['start']).'"
				p_end = "'.$this->DB->filter($_REQUEST['end']).'"
				p_summary = "'.$this->DB->filter($_REQUEST['summary']).'"
				p_notes = "'.$this->DB->filter($_REQUEST['notes']).'"
				WHERE p_id = '.$this->DB->filter($tid).' LIMIT 1';
		if( $this->DB->num_rows( $sql ) > 0 ) {
			$data = $this->DB->get_results( $sql );
			return $data[0];
		} else return false;
		*/
		if(!$_REQUEST['tid']) {
			//echo 'Error: no tid'.$_REQUEST['tid'];
			return false;
		}
		
		$update = array(
		    'p_start' 	=> $this->DB->filter(date("Y-m-d", strtotime($_REQUEST['start']))),
			'p_end' 	=> $this->DB->filter(date("Y-m-d", strtotime($_REQUEST['end']))),
			'p_summary' => $this->DB->filter($_REQUEST['summary']),
			'p_notes' 	=> $this->DB->filter($_REQUEST['notes']),
			'p_priority'=> $this->DB->filter($_REQUEST['priority']),
			'p_status' 	=> $this->DB->filter($_REQUEST['status'])
		);
		//Add the WHERE clauses
		$where_clause = array(
		    'p_id' => $this->DB->filter($_REQUEST['tid'])
		);
		$updated = $this->DB->update( 'projects', $update, $where_clause, 1 );
		if( $updated )
		{
		    return true;//echo '<p>Successfully updated projects</p>';
		} else return false;
	}


	private function examples()
	{
		/**
		 * Retrieve results of a standard query
		 */
		$query = "SELECT group_name FROM example_phpmvc";
		$results = $database->get_results( $query );
		foreach( $results as $row )
		{
		    echo $row['group_name'] .'<br />';
		}


		/**
		 * Retrieving a single row of data
		 */
		$query = "SELECT group_id, group_name, group_parent FROM example_phpmvc WHERE group_name LIKE '%production%'";
		if( $database->num_rows( $query ) > 0 )
		{
		    list( $id, $name, $parent ) = $database->get_row( $query );
		    echo "<p>With an ID of $id, $name has a parent of $parent</p>";
		}
		else
		{
		    echo 'No results found for a group name like &quot;production&quot;';
		}
		
		
		$sql = 'SELECT * FROM content WHERE c_section = "'.$this->DB->filter($this->pathinfo[1]).'" LIMIT 1';
		if( $this->DB->num_rows( $sql ) > 0 ) {
			return $this->DB->get_results( $sql );
		} else return false;

	}









}


?>